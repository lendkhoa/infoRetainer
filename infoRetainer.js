/*
  * @version: Jan 31, 2018
  * @author: Khoa Le
  */

"use strict";
const missed = 0;
const upcoming = 1;
const success = 2;
const wakeupTime = 9;
const nextMileStone = "next-MS";

// Task object
class InfoRetainerTask {
    constructor(name, date) {
        this.util = new Utility();
        this.completed = false;
        this.startTime = date;
        this.passed = false;
        this.name = name;
        this.isScheduled = true;
        this.planType = 0; // 0-memorizeQuickly, 1-memorizeForALongTime
        this.score = 10;
        // 0 - no modification, 1-missed, 2-reviewed
        this.reviewedMilestone = [0,0,0,0,0];

        // milestones
        this.next30mins = new Date(this.util.get30mins(this.startTime)).getTime();
        this.next8hrs = new Date(this.util.get8hrs(this.startTime)).getTime();
        this.next24hrs = new Date(this.util.get24hrs(this.startTime)).getTime();
        this.next2weeks = new Date(this.util.get2weeks(this.startTime)).getTime();
        this.next2months = new Date(this.util.get2months(this.startTime)).getTime();
    }

    getTaskInfo() {
        console.log(`Name: ${this.name}
                    Begin date: ${new Date(this.startTime).toString()}
                    Is scheduled: ${this.isScheduled}`);
    }
}

// Class contains simple, supporting functions
class Utility {

    Utility() {
        this.startTime = new Date().getTime();
    }

    // Returns the number of hours delayed until wake up time-9am
    addDelayHr (startTime) {
        let timeString = new Date(startTime).toString();
        timeString = timeString.substr(timeString.lastIndexOf("GMT") - 9, timeString.indexOf(":") - 16);
        return wakeupTime - parseInt(timeString);
    }

    addMilestoneDisplay (task, status, message, milestone, parent) {
        // missed pass milestone
        if (status == 0) {
            milestone.setAttribute("class", "btn btn-light next-MS");
            milestone.setAttribute("id", task);
            milestone.setAttribute("data-toggle", "modal");
            milestone.setAttribute("data-target", "#exampleModal");
            milestone.appendChild(document.createTextNode(message));
            parent.appendChild(milestone);
        } else if (status == 1) {
            // upcoming milestone
            milestone.setAttribute("class", "btn btn-warning next-MS");
            milestone.setAttribute("id", task);
            milestone.setAttribute("data-toggle", "modal");
            milestone.setAttribute("data-target", "#exampleModal");
            milestone.appendChild(document.createTextNode(message));
            parent.appendChild(milestone);
        } else if (status == 2) {
            milestone.setAttribute("class", "btn btn-secondary next-MS");
            milestone.setAttribute("id", task);
            milestone.setAttribute("data-toggle", "modal");
            milestone.setAttribute("data-target", "#exampleModal");
            milestone.appendChild(document.createTextNode(message));
            parent.appendChild(milestone);
        } else if (status == 3) {
            milestone.setAttribute("class", "btn btn-dark next-MS");
            milestone.setAttribute("id", task);
            milestone.setAttribute("data-toggle", "modal");
            milestone.setAttribute("data-target", "#exampleModal");
            milestone.appendChild(document.createTextNode(message));
            parent.appendChild(milestone);
        } else if (status == 4) {
            milestone.setAttribute("class", "btn btn-danger next-MS");
            milestone.setAttribute("id", task);
            milestone.setAttribute("data-toggle", "modal");
            milestone.setAttribute("data-target", "#exampleModal");
            milestone.appendChild(document.createTextNode(message));
            parent.appendChild(milestone);
        }
    }

    appendMilestoneCheckboxes (task, htmlElement) {
        for (let i = 0; i < task.reviewedMilestone.length; i++) {
            let form = document.createElement("div");
            form.setAttribute("class", "form-check form-check-inline");
            if (i == 0) {
                new Utility().createCheckbox(form, "30 mins", task.next30mins, task.name, i);
            } else if (i == 1) {
                new Utility().createCheckbox(form, "8 hours", task.next8hrs, task.name, i);
            } else if (i == 2) {
                new Utility().createCheckbox(form, "24 hours", task.next24hrs, task.name, i);
            } else if (i == 3) {
                new Utility().createCheckbox(form, "2 weeks", task.next2weeks, task.name, i);
            } else if (i == 4) {
                new Utility().createCheckbox(form, "2 months", task.next2months, task.name, i);
            }

            htmlElement.appendChild(form);
        }
    }

    addMinutes(startTime, min) {
        let afterXmins = startTime + min * 60000;
        return afterXmins;
    }

    // when user click save changes
    addUpdateChangeListener() {
        document.getElementById("updateChanges").addEventListener('click', function(){
            chrome.storage.sync.get("taskList", function(obj){
                let taskName = document.getElementById("exampleModalLabel").textContent;
                taskName = taskName.substr(0, taskName.lastIndexOf("Created"));
                let updatedCheckboxes = [0, 0, 0, 0, 0];
                // check new update to the check boxes
                for (let i = 0; i < 5; i++) {
                    if (document.getElementById(i).disabled) {
                        updatedCheckboxes[i] = 1;
                    }
                    if (document.getElementById(i).checked) {
                        updatedCheckboxes[i] = 2;
                    }
                }
                for (let i = 0; i < obj.taskList.length; i++) {
                    if (obj.taskList[i].name == taskName.trim()) {
                        obj.taskList[i].reviewedMilestone = updatedCheckboxes;
                        console.log(obj.taskList[i].reviewedMilestone);
                    }
                }

                chrome.storage.sync.set({ "taskList": obj.taskList}, function(){
                    location.reload();
                });
            });
        });
    }

    addInputModifyListeners (inputListenerIds) {
        let inputList = document.getElementsByClassName("inputFieldClass");
        for (let i = 0; i < inputList.length; i++) {
            inputList[i].addEventListener("change", function(){
                new Utility().updateTask(inputList[i].id, inputList[i].value);
            });
        }
    }

    addModalListeners(removeListenerIds) {
        let modalList = document.getElementsByClassName("next-MS");
        for (let i = 0; i < modalList.length; i++) {
            modalList[i].addEventListener("click", function(){
                let modal = document.getElementById("exampleModalLabel");
                modal.innerHTML = modalList[i].id;
                // update modal content
                new Utility().updateModalContent(modalList[i].id, modal);
            });
        }
    }

    addRemoveListeners(removeListenerIds) {
        let removeButtonList = document.getElementsByClassName("removeClass");
        for (let i = 0; i < removeButtonList.length; i++) {
            removeButtonList[i].addEventListener("click", function(){
                removeTask(removeButtonList[i].id);
            });
        }
    }

    addRescheduleListeners(rescheduleListenerIds) {
        let rescheduleButtonList = document.getElementsByClassName("rescheduleClass");
        for (let i = 0; i < rescheduleButtonList.length; i++) {
            rescheduleButtonList[i].addEventListener("click", function(){
                new Utility().updateTask("", rescheduleButtonList[i].id);
            });
        }
    }

    appendControlFeature(htmlType, customClass, nodeTextContent, id, parent) {
        // add remove button
        const feature = document.createElement(htmlType);
        feature.setAttribute("class", customClass);
        feature.setAttribute("id", id);

        if (htmlType == "input") {
            feature.value = nodeTextContent;
        }
        else {
            feature.appendChild(document.createTextNode(nodeTextContent));
        }

        parent.appendChild(feature);
    }

    beforeWakeupTime (time) {
        let timeString = new Date(time).toString();
        timeString = timeString.substr(timeString.lastIndexOf("GMT") - 9, timeString.indexOf(":") - 16);
        timeString = parseInt(timeString);
        return parseInt(timeString) < 6 ? true : false;
    }

    checkLimit(content) {
        // initial limit 150 bytes per entry
        let limit = memorySizeOf(content);
        let preparedString = "";
        if (parseInt(limit) > 150) {
            console.log(new Message(limit).sizeExceedsLimit);
            preparedString = this.shrinkString(content);
            return preparedString;
        }
        else {
            return content;
        }
    }

    cleanup(key) {
        chrome.storage.sync.remove([key],function(){
            var error = chrome.runtime.lastError;
            if (error) {
                console.error(error);
            } else {
                console.log(new Message().cleanStorageSuccess);
            }
        });
    }

    createNotification(msg) {
        const completeButton = document.createElement("button");
        let opt = {
            type: 'list',
            title: 'Primary Title',
            message: 'Primary message to display',
            priority: 1,
            iconUrl: './The nail.JPG',
            items: [{ title: 'Item1', message: msg}]
        };

        chrome.notifications.create('notify1', opt, function() { console.log(chrome.runtime.lastError); });
    }

    /**
      * Create checkbox and its description badge
      */
    createCheckbox(form, checkboxMsg, milestoneTime, className, id) {
        let box = document.createElement("input");
        box.setAttribute("class", `form-check-input`);
        box.setAttribute("type", "checkbox");
        box.setAttribute("id", id);

        let badge = document.createElement("div");
        badge.setAttribute("class", "badge badge-success");
        badge.appendChild(document.createTextNode(checkboxMsg));
        box.setAttribute("class", "form-check-input");
        box.setAttribute("type", "checkbox");

        let time = document.createElement("h6");
        let timeStr = new Date(milestoneTime).toString();

        // if the deadline is before the wakeup time at 6am - delay until 6
        if (new Utility().beforeWakeupTime(milestoneTime)) {
            let delayHr = this.addDelayHr(milestoneTime);
            time.appendChild(document.createTextNode(`${timeStr.substr(0, timeStr.lastIndexOf("GMT"))}
                            is delayed for ${delayHr} hour(s) until ${wakeupTime} am`));
        } else {
            time.appendChild(document.createTextNode(timeStr.substr(0, timeStr.lastIndexOf("GMT"))));
        }

        form.appendChild(box);
        form.appendChild(badge);
        form.appendChild(time);
    }

    getNumberOfDaysRemaining(remainingMilliseconds) {
        return (remainingMilliseconds / 86400000).toFixed(0);
    }

    getTestingObj() {
        let testingObj = [];
        let feb12010 = 1518868912089;
        const obj1 = new InfoRetainerTask("Read c++", feb12010);
        const obj2 = new InfoRetainerTask("Java api", 1265004008390);
        const obj3 = new InfoRetainerTask("Before 8am", new Date());

        testingObj.push(obj1);
        testingObj.push(obj2);
        testingObj.push(obj3);
        return testingObj;
    }

    /**
      * Returns the number of milleseconds of 20 minutes after start time
      */
    get20mins(startTime) {
        return this.addMinutes(startTime, 20);
    }

    get30mins(startTime) {
        return this.addMinutes(startTime, 30);
    }

    /**
      * Returns the number of milleseconds of 8 hours
      * after start time - or 8 hours + delayHr if the
      * deadline is before wake-up time.
      */
    get8hrs(startTime) {
        if (this.beforeWakeupTime(startTime)) {
            let delayHr = this.addDelayHr(startTime);
            return this.addMinutes(startTime, 480 + delayHr * 60);
        } else {
            return this.addMinutes(startTime, 480);
        }
    }

    get24hrs(startTime) {
        if (this.beforeWakeupTime(startTime)) {
            let delayHr = this.addDelayHr(startTime);
            return this.addMinutes(startTime, 1440 + delayHr * 60);
        } else {
            return this.addMinutes(startTime, 1440);
        }
    }

    get2weeks(startTime) {
        if (this.beforeWakeupTime(startTime)) {
            let delayHr = this.addDelayHr(startTime);
            return this.addMinutes(startTime, 20160 + delayHr * 60);
        } else {
            return this.addMinutes(startTime, 20160);
        }
    }

    get2months(startTime) {
        if (this.beforeWakeupTime(startTime)) {
            let delayHr = this.addDelayHr(startTime);
            return this.addMinutes(startTime, 86400 + delayHr * 60);
        } else {
            return this.addMinutes(startTime, 86400);
        }
    }

    shrinkString(content) {
        while(parseInt(memorySizeOf(content)) > 150) {
            content = content.substring(0, content.length-1);
        }
        return content;
    }

    updateTaskStatus(task) {
        chrome.storage.sync.get("taskList", function(obj){
            for (let i = 0; i < obj.taskList.length; i++) {
                if (obj.taskList[i].name == task.name) {
                    obj.taskList[i].completed = true;
                }
            }
            chrome.storage.sync.set({ "taskList": obj.taskList}, function(){
            });
        });
    }

    updateTask(archive, taskName) {
        chrome.storage.sync.get("taskList", function(obj){
            // if (obj.taskList.length == 0)
            for (let i = 0; i < obj.taskList.length; i++) {
                // if no archive, create new task
                if (archive == "") {
                    if (obj.taskList[i].name == taskName) {
                        obj.taskList[i] = new InfoRetainerTask(taskName, new Date().getTime());
                    }
                } else {
                    // if there is archive - it means updating the current task
                    if (obj.taskList[i].name == archive) {
                        obj.taskList[i].name = taskName;
                    }
                }
            }
            chrome.storage.sync.set({"taskList": obj.taskList}, function(){
                location.reload();
            });
        });
    }

    updateModalContent (modalName, htmlElement) {
        chrome.storage.sync.get("taskList", function(obj){
            let util = new Utility();
            for (let i = 0; i < obj.taskList.length; i++) {
                if (obj.taskList[i].name == modalName) {
                    obj.taskList[i].completed = true;
                    let badge = document.createElement("div");
                    badge.setAttribute("class", "badge badge-dark");
                    badge.appendChild(document.createTextNode(` Created on: ${new Date(obj.taskList[i].startTime)}`));
                    htmlElement.appendChild(badge);

                    // add milestone checkboxes
                    let modalBody = document.getElementById("modalBody");
                    modalBody.innerHTML = "";
                    new Utility().appendMilestoneCheckboxes(obj.taskList[i], modalBody);
                    // clear old check box
                    for (let j = 0; j < obj.taskList[i].reviewedMilestone.length; j++) {
                        document.getElementById(j).checked = false;
                    }
                    // update new checkbox status
                    let disabledBoxIndexes = [];
                    for (let j = 0; j < obj.taskList[i].reviewedMilestone.length; j++) {
                        // load the checked boxes from the db
                        if (obj.taskList[i].reviewedMilestone[j] == 2) {
                            document.getElementById(j).checked = true;
                        }
                        // grey out the missed boxes if not check in 10 mins after deadline
                        if ((j == 0 && util.addMinutes(obj.taskList[i].next30mins, 10) < new Date().getTime())
                           || obj.taskList[i].reviewedMilestone[j] == 1) {
                            document.getElementById(j).disabled = true;
                            disabledBoxIndexes.push(j);
                        } else if ((j == 1 && util.addMinutes(obj.taskList[i].next8hrs, 10) < new Date().getTime())
                            || obj.taskList[i].reviewedMilestone[j] == 1) {
                            document.getElementById(j).disabled = true;
                            disabledBoxIndexes.push(j);
                        } else if ((j == 2 && util.addMinutes(obj.taskList[i].next24hrs, 10) < new Date().getTime())
                                  || obj.taskList[i].reviewedMilestone[j] == 1) {
                            document.getElementById(j).disabled = true;
                            disabledBoxIndexes.push(j);
                        } else if ((j == 3 && util.addMinutes(obj.taskList[i].next2weeks, 10) < new Date().getTime())
                                   || obj.taskList[i].reviewedMilestone[j] == 1) {
                            document.getElementById(j).disabled = true;
                            disabledBoxIndexes.push(j);
                        } else if ((j == 4 && util.addMinutes(obj.taskList[i].next2months, 10) < new Date().getTime())
                                   || obj.taskList[i].reviewedMilestone[j] == 1) {
                            document.getElementById(j).disabled = true;
                            disabledBoxIndexes.push(j);
                        }
                    }
                    // reduce point at index of disabled boxes
                    for (let j = 0; j < disabledBoxIndexes.length; j++) {
                        if (obj.taskList[i].reviewedMilestone[disabledBoxIndexes[j]] == 0) {
                            let tempScore = obj.taskList[i].score - 1.67;
                            obj.taskList[i].reviewedMilestone[j] = 1;
                            obj.taskList[i].score = tempScore > 0 ? tempScore.toFixed(2) : 0;
                        }
                    }
                    // save updated score
                    chrome.storage.sync.set({"taskList": obj.taskList}, function(){
                    });
                }
            }
        });
    }
}


/*
 * EXTERNAL RESOURCES
 * @author: https://github.com/zensh
 */
function memorySizeOf(obj) {
    var bytes = 0;

    function sizeOf(obj) {
        if(obj !== null && obj !== undefined) {
            switch(typeof obj) {
            case 'number':
                bytes += 8;
                break;
            case 'string':
                bytes += obj.length * 2;
                break;
            case 'boolean':
                bytes += 4;
                break;
            case 'object':
                var objClass = Object.prototype.toString.call(obj).slice(8, -1);
                if(objClass === 'Object' || objClass === 'Array') {
                    for(var key in obj) {
                        if(!obj.hasOwnProperty(key)) continue;
                        sizeOf(obj[key]);
                    }
                } else bytes += obj.toString().length * 2;
                break;
            }
        }
        return bytes;
    };

    function formatByteSize(bytes) {
        if(bytes < 1024) return bytes + " bytes";
        else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KiB";
        else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MiB";
        else return(bytes / 1073741824).toFixed(3) + " GiB";
    };

    return formatByteSize(sizeOf(obj));
};

// w3mSchool website
function searchTable() {
    // Declare variables
    let foundMatch = false;
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
                foundMatch = true;
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function generateMilestone (task, htmlElement) {
    const currentTime = new Date().getTime();
    const util = new Utility();
    util.appendControlFeature("span", "badge badge-info", `Score: ${task.score}`, task.name, htmlElement);

    if (currentTime < task.next30mins) {
        const min30 = document.createElement("button");
        util.addMilestoneDisplay (task.name, 0, "Next review milestone is in 30 mins", min30, htmlElement);

    } else if (currentTime > task.next30mins && currentTime < task.next8hrs) {
        const hr8 = document.createElement("button");
        util.addMilestoneDisplay (task.name, 1, "Next review milestone is in 8 hours", hr8, htmlElement);

    } else if (currentTime > task.next8hrs && currentTime < task.next24hrs) {
        const hr24 = document.createElement("button");
        util.addMilestoneDisplay (task.name, 2, "Next review milestone is in 24 hours", hr24, htmlElement);

    } else if (currentTime > task.next24hrs && currentTime < task.next2weeks) {
        const wk2 = document.createElement("button");
        util.addMilestoneDisplay (task.name, 3, `Next review milestone is in ${util.getNumberOfDaysRemaining(task.next2weeks - currentTime)} day(s)`, wk2, htmlElement);

    } else if (currentTime > task.next2weeks && currentTime < task.next2months) {
        const mt2 = document.createElement("button");
        util.addMilestoneDisplay (task.name, 4, `Next review milestone is in ${util.getNumberOfDaysRemaining(task.next2months - currentTime)} day(s)`, mt2, htmlElement);

    } else if (currentTime > task.next2months) {
        const reschedule = document.createElement("button");
        util.updateTaskStatus(task);

        // Allow rescheduling the current task htmlElement.setAttribute("style", "text-decoration: line-through;");
        reschedule.setAttribute("class", "btn btn-outline-primary rescheduleClass");
        reschedule.setAttribute("data-toggle", "modal");
        reschedule.setAttribute("data-target", "#exampleModal");
        reschedule.setAttribute("id", `${task.name}`);
        reschedule.appendChild(document.createTextNode("Reschedule"));
        htmlElement.appendChild(reschedule);

    }
    // add remove button
    util.appendControlFeature("button", "btn btn-outline-danger removeClass", "Remove", task.name, htmlElement);
}

function removeTask(taskName) {
    chrome.storage.sync.get("taskList", function(obj){
        // if (obj.taskList.length == 0)
        for (let i = 0; i < obj.taskList.length; i++) {
            if (obj.taskList[i].name == taskName) {
                obj.taskList.splice(i, 1);
            }
        }
        chrome.storage.sync.set({"taskList": obj.taskList}, function(){
            location.reload();
        });
    });
}

function updatePageContent() {
    chrome.storage.sync.get("taskList", function(obj){
        // if list is empty - clear table
        const util = new Utility();
        let rescheduleListenerIds = [];
        let tbl = document.getElementById("myTable");
        if (obj.taskList.length == 0) {
            if (tbl) tbl.parentNode.removeChild(tbl);
        } else {
            let tbl = document.getElementById("myTable");
            let scheduledTasks = obj.taskList;
            let totalScore = 0;
            for (let i = 0 ; i < scheduledTasks.length; i++) {
                const curTask = scheduledTasks[i];
                // memorizeQuicly
                if (curTask.planType == 0) {
                    let tr = document.createElement("tr");
                    let td = document.createElement("td");

                    // append task name to input field
                    util.appendControlFeature("input", "inputFieldClass", curTask.name, curTask.name, td);

                    // append pass and upcoming milestones
                    generateMilestone(curTask, td);
                    totalScore += parseFloat(curTask.score);

                    tr.appendChild(td);
                    tbl.appendChild(tr);

                    // add this id to listener schedule
                    rescheduleListenerIds.push(curTask.name);
                } else {
                    // TODO add feature for memorize for a long time
                }
            }
            // update total score
            let scoreNode = document.createTextNode(`Total score: ${totalScore.toFixed(2)}`);
            let scoreBanner = document.createElement("h2");
            scoreBanner.setAttribute("id", "totalScore");
            scoreBanner.setAttribute("class", "badge badge-success");
            scoreBanner.appendChild(scoreNode);
            document.getElementById("taskList").appendChild(scoreBanner);
        }
        // add listeners
        util.addRescheduleListeners(rescheduleListenerIds);
        util.addUpdateChangeListener();
        util.addRemoveListeners(rescheduleListenerIds);
        util.addModalListeners(rescheduleListenerIds);
        util.addInputModifyListeners(rescheduleListenerIds);
    });
}

function setup() {
    document.getElementById("myInput").focus();
    document.getElementById("myInput").select();

    // create task listener
    document.getElementById("createNewTask").addEventListener("click", function() {
        const newTask = document.getElementById("myInput").value;
        if (newTask) {
            chrome.storage.sync.get("taskList", function(obj){
                // sync storage has a list
                if (obj.taskList) {
                    const newlyCreatedTask = new InfoRetainerTask(newTask, new Date().getTime());
                    obj.taskList.push(newlyCreatedTask);
                    chrome.storage.sync.set({ "taskList": obj.taskList}, function(){
                        location.reload();
                    });
                } else {
                    let taskList = [];
                    const temp = new InfoRetainerTask(newTask, new Date().getTime());
                    taskList.push(temp);
                    chrome.storage.sync.set({ "taskList": taskList}, function(){
                        location.reload();
                    });
                }
            });
        } else {
            chrome.storage.sync.get("taskList", function(obj){
                if (obj.taskList) {
                    location.reload();
                }
            });
        }
    });

    // Search bar listener
    document.getElementById("myInput").addEventListener("keyup", function() {
        searchTable();
    });

    // Load button listener
    document.getElementById("loadStorage").addEventListener("click", function(){
        chrome.storage.sync.get("taskList", function(obj) {
            if (obj.taskList) {
                console.log(obj);
            } else {
                console.log("List is empty");
            }
        });
    });

}

window.onload = function() {
    function initLoadTest() {
        chrome.storage.sync.set({ "taskList": util.getTestingObj()}, function(){
            chrome.storage.sync.get("taskList", function(obj) {
                if (obj.taskList) {
                } else {
                    console.log("List is empty");
                }
            });
        });
    }

    // Functions to load testing object
    // initLoadTest();
    setup();
    updatePageContent();
};
